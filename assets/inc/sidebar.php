<aside class="col-md-4">

	<div class="laread-right">

		<form class="laread-form search-form">
			<div class="input"><input type="text" class="form-control" placeholder="Search..."></div>
			<button type="submit" class="btn btn-link"><i class="fa fa-search"></i></button>
		</form>

		<ul class="laread-list">
			<li class="title">CATEGORY</li>
			<li><a href="#">Branding</a><i class="line"></i></li>
			<li><a href="#">Design (48)</a><i class="line"></i></li>
			<li><a href="#">Photography</a><i class="line"></i></li>
			<li><a href="#">Inspiration</a><i class="line"></i></li>
			<li><a href="#">Life</a><i class="line"></i></li>
			<li><a href="#">City</a><i class="line"></i></li>
		</ul>

		<ul class="laread-list">
			<li class="title">RECENT POSTS</li>
			<li><a href="#">The Nature of My Inspiration</a><i class="date">28 June</i></li>
			<li><a href="#">Sam Feldt - Show Me Love</a><i class="date">27 June</i></li>
			<li><a href="#">Do You Love Coffee?</a><i class="date">25 June</i></li>
			<li><a href="#">The Game Before The Game</a><i class="date">23 June</i></li>
			<li><a href="#">Long Live The Kings</a><i class="date">22 June</i></li>
		</ul>

		<ul class="laread-list">
			<li class="title">TAGS</li>
			<li class="bar-tags">
				<a href="#">fashion</a>
				<a href="#">culture</a>
				<a href="#">art</a>
				<a href="#">concept</a>
				<a href="#">style</a>
				<a href="#">advert</a>
				<a href="#">movie</a>
				<a href="#">color</a>
				<a href="#">branding</a>
				<a href="#">technology</a>
				<a href="#">fashion</a>
				<a href="#">culture</a>
				<a href="#">art</a>
				<a href="#">concept</a>
			</li>
		</ul>

		<ul class="laread-list barbg-grey">
			<li class="title">NEWSLETTER</li>
			<li class="newsletter-bar">
				<p>Vivamus nec mauris pulvinar leo dignissim sollicitudin eleifend eget velit.</p>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
					<input type="text" class="form-control" placeholder="john@doe.com">
					<span class="input-group-btn">
						<button class="btn" type="button"><i class="fa fa-check"></i></button>
					</span>
				</div>
			</li>
		</ul>

		<div class="laread-list quotes-basic">
			<i class="fa fa-quote-left"></i>
			<p>“The difference between stupidity and genius is that genius has its limits.”</p>
			<span class="whosay">- Albert Einstein </span>
		</div>

		<ul class="laread-list social-bar">
			<li class="title">FOLLOW US</li>
			<li class="social-icons">
				<a href="#"><i class="fa fa-facebook"></i></a>
				<a href="#"><i class="fa fa-twitter"></i></a>
				<a href="#"><i class="fa fa-google-plus"></i></a>
				<a href="#"><i class="fa fa-dribbble"></i></a>
			</li>
		</ul>

	</div>

</aside>