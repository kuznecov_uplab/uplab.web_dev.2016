<?$items = include __DIR__.'/data/news.php';?>


<?foreach ($items as $key => $item):?>

<div class="container-fluid post-default">
	<div class="container-medium">
		<div class="row post-items">
			<div class="post-item-banner">
				<img src="/assets/img/banner-40.jpg" alt="" />
			</div>
			<div class="col-md-12">
				<div class="post-item">
					<div class="post-item-paragraph">
						<div>
							<a href="#" class="quick-read qr-only-phone"><i class="fa fa-eye"></i></a>
							<a href="#" class="mute-text">DESIGN</a>
						</div>
						<h3><a href="#">Meet #59 Interface Designer John Doe</a></h3>
						<p>Praesent mollis sodales est, eget fringilla libero sagittis eget. Nunc gravida varius risus ac luctus. Mauris ornare eros sed libero euismod ornare. Nulla id sem a mauris egestas pulvinar vitae non dui. Cras odio tortor, feugiat nec sagittis sed, laoreet ut mauris. In hac habitasse platea dictumst. Mauris non libero ligula, sed volutpat mauris <a href="#" class="more">[...]</a></p>
					</div>
					<div class="post-item-info clearfix">
						<div class="pull-left">
							<span>28 June</span>   •   By <a href="#">Daniele Zedda</a>
						</div>
						<div class="pull-right post-item-social">
							<a href="#" class="quick-read qr-not-phone"><i class="fa fa-eye"></i></a>
							<a href="#" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="<a href='#'><i class='fa fa-facebook'></i></a><a href='#'><i class='fa fa-twitter'></i></a>" class="pis-share"><i class="fa fa-share-alt"></i></a>
							<a href="#" class="post-like"><i class="fa fa-heart"></i><span>28</span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?endforeach;?>